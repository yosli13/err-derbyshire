# err-derbyshire

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Plugin para [errbot](https://errbot.readthedocs.io/en/latest/), copia del [bot de hospitalidad del CLiC](https://gitlab.com/colectivo-de-livecoders/telegram-bot/).

## Prerequisitos

Tener un errbot funcionando.

## Instalar

```
!repos install https://gitlab.com/jaquerespeis/err-derbyshire
```

## Pruebas


1. Instalar python y venv:

```
sudo apt install python3 python3-venv
```

2. Hacer y activar el venv:

En el directorio superior por
https://github.com/errbotio/errbot/issues/1412#issuecomment-679622186

```
python3 -m venv ../.venv-err-derbyshire
source ../.venv-err-derbyshire/bin/activate
```

3. Instalar las dependencias:

```
pip install errbot testscenarios pytest
```

4. Ejecutar las pruebas:

```
python3 -m unittest discover tests/
```

## Mantenedor

* [elopio](https://keybase.io/elopio)

## Contribuir

TODO

## Licencia

[GPLv3](LICENSE) (Copyleft) [CLiC](https://colectivo-de-livecoders.gitlab.io/) y [JáquerEspeis](https://bunqueer.jaquerespeis.org)
